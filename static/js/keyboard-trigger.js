const KT_MAX_STACK_LEN = 32;

const __kt_keywords = {};
let __kt_stack = [];

function kt_register(string, callback) {
    __kt_keywords[string] = callback;
}

function kt_delete(string) {
    delete __kt_keywords[string];
}

addEventListener('keypress', (e) => {
    if (e.key === 'Enter' || e.key === ' ')
        return;

    __kt_stack.push(e.key.toLowerCase());

    const joined_stack = __kt_stack.join('');
    for (key of Object.keys(__kt_keywords)) {
        if (!joined_stack.endsWith(key))
            continue;

        setTimeout(__kt_keywords[key], 0);
        __kt_stack = [];

        break;
    }

    const stack_len = __kt_stack.length;
    if (stack_len > KT_MAX_STACK_LEN)
        __kt_stack.splice(0, stack_len / 2);
});
