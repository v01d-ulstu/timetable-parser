fetch('/api/clients/android').then(async (res) => {
    const data = await res.json()
    android_app_info.innerHTML = `
        Версия: ${data.version}
        <br>
        Всего загрузок: ${data.downloaded_total}
        <br>
        Загрузок текущей версии: ${data.downloaded_curver}
    `
})


update.onclick = (event) => {
    event.preventDefault()
    fetch('/api/update-androidapp', {
        method: 'POST',
        body: new FormData(fileUpload)
    })
    .then(response => response.text())
    .then(success => alert(success))
    .catch(error => alert(error))
}

kt_register('iamadmin', () => {
    uploadForm.hidden = false
})

new fullpage('#fullpage', {
    //options here
    anchors: ['page1', 'page2'],
    autoScrolling:true,
    scrollHorizontally: true,
    verticalCentered: false,
    navigation: true,
    paddingBottom: '10px'
});

kt_register('meme', () => {
    for (let meme of document.querySelectorAll('.meme')) {
        meme.hidden = false
    }
})

