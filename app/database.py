import sqlite3

import config
import utils


SQL_SCHEMA = f"""
DROP TABLE IF EXISTS clients;
DROP TABLE IF EXISTS admin;

PRAGMA foreign_keys=on;

CREATE TABLE clients (
    platform TEXT NOT NULL,
    version TEXT NOT NULL,
    file TEXT NOT NULL,
    downloaded_total INTEGER NOT NULL,
    downloaded_curver INTEGER NOT NULL
);

CREATE TABLE admin (
    password_sha512 TEXT NOT NULL
);

INSERT INTO clients VALUES ("android", "0.0.0", "", 0, 0);
INSERT INTO admin VALUES ("{config.DATABASE_DEFAULT_PASSWORD_SHA512}");
"""


class DBConnection:
    def __init__(self, file=config.DATABASE_FILE):
        self.__connection = sqlite3.connect(file)
        self.cursor = self.__connection.cursor()

    def __del__(self):
        self.__connection.close()

    def exec(self, *args, **kwargs):
        return self.cursor.execute(*args, **kwargs)

    def commit(self):
        self.__connection.commit()


def create_and_init():
    print('USING SCHEMA:', SQL_SCHEMA, '...', sep='\n')
    db = DBConnection()
    db.cursor.executescript(SQL_SCHEMA)
    db.commit()
    print('OK!')


def update_androidapp(version: str, url: str, db: DBConnection=None):
    if db is None:
        db = DBConnection()

    db.exec('UPDATE clients '
            'SET version = ?, file = ?, downloaded_curver = 0 '
            'WHERE platform = "android"',
            (version, url))
    db.commit()


def increment_androidapp_download_count(db: DBConnection=None):
    if db is None:
        db = DBConnection()

    db.exec('UPDATE clients '
            'SET '
            'downloaded_total = downloaded_total + 1, '
            'downloaded_curver = downloaded_curver + 1 '
            'WHERE platform = "android"')
    db.commit()


def get_platform_info(platform: str, db: DBConnection=None):
    if db is None:
        db = DBConnection()

    res = db.exec('SELECT version, file, downloaded_total, '
                  'downloaded_curver FROM clients WHERE platform = ?',
                  (platform,)).fetchone()

    if res is None:
        raise ValueError('Platform not found')

    return {
        'platform': platform,
        'version': res[0],
        'file': res[1],
        'downloaded_total': res[2],
        'downloaded_curver': res[3],
    }


def get_admin_password_sha512(db: DBConnection=None):
    if db is None:
        db = DBConnection()
    
    return db.exec('SELECT password_sha512 FROM admin').fetchone()[0]


def set_admin_password(password, db: DBConnection=None):
    if db is None:
        db = DBConnection()

    db.exec('UPDATE admin SET password_sha512 = ?', (utils.hash_password(password),))
    db.commit()
