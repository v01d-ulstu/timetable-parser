import os
import utils


# Путь к файлу базы данных
DATABASE_FILE = os.path.abspath('utp.db.sqlite3')

# Пароль админа по умолчанию. ТОЛЬКО ДЛЯ РАЗРАБОТКИ И ТЕСТИРОВАНИЯ!
# На проде перезаписать! 
DATABASE_DEFAULT_PASSWORD_SHA512 = utils.hash_password('devpwd')

# Путь к папке с фронтом
FRONTEND_STATIC_FOLDER = os.path.abspath('static')

# Базовая часть названия приложения андроид {BASE}_VERSION.apk
MOBILE_APP_ANDROID_FILEBASE = 'sced'

# Папка с apk'шками
MOBILE_APP_ANDROID_FOLDER = os.getcwd()

# Удалять ли старую версию при обновлении приложения
MOBILE_APP_ANDROID_REMOVE_WHEN_UPDATE = True

# Время кэша файла, 16 минут
MOBILE_APP_ANDROID_CACHE_TIMEOUT = 16 * 60
