import os

import flask

import database
import utils
import config


api = flask.Blueprint('api', __name__)


@api.after_request
def set_crossdomain_headers(response):
    """This middleware provides cross-domain requests"""
    response.headers['Access-Control-Allow-Origin'] = '*'
    return response


@api.route('/clients/<platform>')
def api_client_info(platform):
    try:
        client_info = database.get_platform_info(platform)
    except ValueError:
        flask.abort(404)

    return flask.jsonify(client_info)


@api.route('/download-androidapp')
def download_androidapp():
    db = database.DBConnection()
    database.increment_androidapp_download_count(db)
    filename = database.get_platform_info('android', db)['file']

    try:
        return flask.send_file(
            os.path.join(config.MOBILE_APP_ANDROID_FOLDER, filename),
            as_attachment=True,
            cache_timeout=config.MOBILE_APP_ANDROID_CACHE_TIMEOUT
        )
    except FileNotFoundError:
        flask.abort(404)


@api.route('/update-androidapp', methods=['POST'])
def api_update_androidapp():
    try:
        password = flask.request.form['password'].strip()
        version = flask.request.form['version'].strip()
        apk = flask.request.files['apk']
    except KeyError:
        flask.abort(400)

    db = database.DBConnection()

    if utils.hash_password(password) != database.get_admin_password_sha512(db):
        flask.abort(403)

    if config.MOBILE_APP_ANDROID_REMOVE_WHEN_UPDATE:
        old_apk_file = os.path.join(config.MOBILE_APP_ANDROID_FOLDER,
                                    database.get_platform_info('android')['file'])
        if os.path.exists(old_apk_file) and os.path.isfile(old_apk_file):
            os.remove(old_apk_file)

    apk_filename = f'{config.MOBILE_APP_ANDROID_FILEBASE}_{version}.apk'

    database.update_androidapp(version, apk_filename, db)
    apk.save(os.path.join(config.MOBILE_APP_ANDROID_FOLDER, apk_filename))

    return 'OK'
