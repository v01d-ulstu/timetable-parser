import flask

import api
import views
import config


app = flask.Flask(__name__, static_folder=config.FRONTEND_STATIC_FOLDER)
app.register_blueprint(api.api, url_prefix='/api/')
app.register_blueprint(views.views)
