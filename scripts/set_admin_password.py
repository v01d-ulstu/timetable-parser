import getpass
import sys
sys.path.append('app')

import database


new_password = getpass.getpass()
new_password_confirmation = getpass.getpass('Repeat: ')

if new_password != new_password_confirmation:
    print('Password mismatch')
else:
    database.set_admin_password(new_password)
    print('OK')
