# ULSTU Timetable Parser

## Что это такое?
Все знают, что на сайте УлГТУ нет API,
приложение с расписанием из Google Play, кажется, уже
никто не поддерживает. Этот проект призван исправить ситуацию.

## Авторы:
- v01d (back-end, Великодушный пожизненный диктатор), https://vk.com/v01d_vk
- Magik21 (Mobile app), https://vk.com/magik_21
- nik0n (front-end), https://vk.com/aleks_two
- Ervincar (front-end), https://vk.com/ervincar

## Документация к API
Префикс: `/api/`  

GET `/clients/<platform>` — Информация о клиенте: статистика скачивания, название файла, версия   
GET `/download-androidapp` — Правильный вариант скачивания, обновляющий статистику загрузок  
POST `/update-androidapp` — Обновление приложения (для админов онли, очевидно):  
Data:  
    - password  
    - version  
    - apk файл  

## Как развернуть backend локально
Следует использовать готовые скрипты, находясь в корне проекта  

Внимание! Python должен быть в PATH, *.py должны открываться Python'ом по умолчанию

1. Подготовить виртуальное окружение Python (всегда)
```cmd
> scripts\prepare_venv
```
2. Подготовить базу данных (только при первом запуске)
```cmd
> scripts\init_db
```
3. Запустить
```cmd
> scripts\run
```
### Кстати...
Стандартный пароль админа указан в app/config.py.  
Это `devpwd`.  Поменять его можно, запустив
```cmd
> scripts\set_admin_password
```
